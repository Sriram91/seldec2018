package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLeadPage;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Verify the test case need to create a Lead";
		author = "Sriram";
		category = "smoke";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void CreateLead1(String uName, String pwd,String cname, String Fname, String Lname)	
	{
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLeads()
		.EnterCompanyName(cname)
		.EnterFirstName(Fname)
		.EnterLastName(Lname)
		.ClickCreateLead();
		
			
	}

}
