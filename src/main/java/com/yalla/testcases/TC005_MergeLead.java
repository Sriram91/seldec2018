package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.FindLeadPage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC005_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DuplicateLead";
		testcaseDec = "Verify the test case need to merge a lead";
		author = "Sriram";
		category = "smoke";
		excelFileName = "TC005";
	} 

	@Test(dataProvider="fetchData") 
	public void MergeLead(String uName, String pwd, String Fname, String data1)	
	{
	
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLead()
		.clickFirstWindow()
        .EnterFirstData(Fname)
		.clickFindLeadButton()
		.clickFirstresults1() 
		.clickSecondWindow()
		.EnterFirstName(data1)
		.clickFindLeadButton()
		.clickSecondresults()
		.clickMergebutton();
	}
			
	}












