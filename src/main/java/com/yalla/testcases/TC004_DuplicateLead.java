package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.FindLeadPage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DuplicateLead";
		testcaseDec = "Verify the test case need to create a duplicate lead";
		author = "Sriram";
		category = "smoke";
		excelFileName = "TC004";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd, String Fname)	
	{
	
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads();
		new FindLeadPage()
		.clickFindLeads()
		.EnterFirstName(Fname)
		.clickFindLeadButton()
		.clickFirstresults()
		.DuplicateLink()
		.clickCreateLeadDup();
		
	}	
			
	}

