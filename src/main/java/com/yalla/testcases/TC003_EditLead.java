package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.FindLeadPage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.ViewLeadPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Verify the test case need to update a Lead";
		author = "Sriram";
		category = "smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd, String Fname,String updatedName)	
	{
	
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads();
		new FindLeadPage()
		.clickFindLeads()
		.EnterFirstName(Fname)
		.clickFindLeadButton()
		.clickFirstresults()
		.clickEditLink()
		.UpdateCompanyName(updatedName)
		.clickUpdateButton();
		
	}	
			
	}

