package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsCRMPage extends Annotations{ 

	public OpenTapsCRMPage() {
       PageFactory.initElements(driver, this);
	} 
	
@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleUpdateCompanyName;
	
	public OpenTapsCRMPage UpdateCompanyName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleUpdateCompanyName, data);
		return this;
	}
		
		@FindBy(how=How.NAME, using="submitButton")  WebElement eleUpdateButton;
		
		
		public ViewLeadPage clickUpdateButton() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleUpdateButton);  
			return new ViewLeadPage();

	}
	}









