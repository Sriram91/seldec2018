package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEditLink;
	public OpenTapsCRMPage clickEditLink()
	{
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleEditLink);  
		return new OpenTapsCRMPage();
	}

@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDuplicateLink;
public DuplicateLeadPage DuplicateLink()
{
	//WebElement eleLogout = locateElement("class", "decorativeSubmit");
	click(eleDuplicateLink);  
	return new DuplicateLeadPage();
}
}

	/*@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();*/

	









