package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.When;

public class FindLeadPage extends Annotations{ 

	public FindLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	
@FindBy(how=How.LINK_TEXT, using="Find Leads")  WebElement eleFindLeads;
	@When ("I Click lead tab in the find lead page")
	public FindLeadPage clickFindLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLeads);  
		return this;


	}
@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]")  WebElement eleEnterFirstName;
	
	public FindLeadPage EnterFirstName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleEnterFirstName, data);
		return this;
	}
	
@FindBy(how=How.XPATH, using="//input[@name='firstName']")  WebElement eleEnterFirstData;
	public FindLeadPage EnterFirstData(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleEnterFirstData, data);
		return this;
	}
	
	
	
@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")  WebElement eleclickFindLeadButton;
	
	public FindLeadPage clickFindLeadButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleclickFindLeadButton);
		return this;
	}	
	
@FindBy(how=How.LINK_TEXT, using="Sriram")  WebElement eleFindresults;
	
	public ViewLeadPage clickFirstresults() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindresults);  
		return new ViewLeadPage();
	}
		
		@FindBy(how=How.LINK_TEXT, using="Sriram")  WebElement eleFindresults1;
			
			public MergeLeadPage clickFirstresults1() {
				//WebElement eleLogout = locateElement("class", "decorativeSubmit");
				click(eleFindresults1); 
				switchToWindow(0);
				return new MergeLeadPage();
				
			}
				
				@FindBy(how=How.XPATH, using="(//a[text()='Sriram'])[2]")  WebElement eleSecondresults;
				
				public MergeLeadPage clickSecondresults() {
					//WebElement eleLogout = locateElement("class", "decorativeSubmit");
					click(eleSecondresults); 
					switchToWindow(0);
					return new MergeLeadPage();

	}

}







