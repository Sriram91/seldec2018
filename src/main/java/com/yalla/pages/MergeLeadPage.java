package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadPage extends Annotations{ 

	public MergeLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.XPATH, using="(//img[@alt='Lookup'])[1]") WebElement eleFristWindow;
	public FindLeadPage clickFirstWindow()
	{
		click(eleFristWindow);  
		switchToWindow(1);
		return new FindLeadPage();
	}
	/*@FindBy(how=How.XPATH, using="//img[@alt='Lookup']") WebElement eleactualWindow;
	public MergeLeadPage clickactualWindow()
	{
		click(eleactualWindow);  
		switchToWindow(0);
		return this;
	}*/
	@FindBy(how=How.XPATH, using=("(//img[@alt='Lookup'])[2]")) WebElement eleSecondWindow;
	public FindLeadPage clickSecondWindow()
	{
		click(eleSecondWindow); 
		switchToWindow(1);
		return new FindLeadPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleMergeButton;
	public ViewLeadPage clickMergebutton()
	{
		click(eleMergeButton);  
		acceptAlert();
		return new ViewLeadPage();
	}
}
	
	






