package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement elecompanyName ;
	@And("I enter the company name (.*)")
	public CreateLeadPage EnterCompanyName(String data) 
	{
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(elecompanyName,data);
		return new CreateLeadPage();
}
	
		@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
		@And("I enter the first name (.*)")
		public CreateLeadPage EnterFirstName(String data)
		{
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(eleFirstName, data);  
			return new CreateLeadPage();
		}
			@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;	
			@When("I enter the last name (.*)")
			public CreateLeadPage EnterLastName(String data) 
			{
				//WebElement eleLogout = locateElement("class", "decorativeSubmit");
				clearAndType(eleLastName, data);
				return new CreateLeadPage();
			}
			
			@FindBy(how=How.NAME, using="submitButton") WebElement eleCreateLead;
			@When("I click Create lead")
			public ViewLeadPage ClickCreateLead() 
			{
				//WebElement eleLogout = locateElement("class", "decorativeSubmit");
				click(eleCreateLead);
				return new ViewLeadPage();
			}
				

}







